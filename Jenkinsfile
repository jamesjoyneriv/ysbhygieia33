#!/usr/bin/env groovy

pipeline {
    agent {
        label 'master'
    }
    stages {
        stage('Compile') {
            steps {
		sh 'hostname'
		sh 'ls -al'
                sh 'mvn -B -DskipTests clean package'
		sh 'mvn compile'
		sh 'ls -al'
            }
        }
        stage('Unit Test') {
            steps {
                sh 'mvn test'
		sh 'ls -al'
            }
            post {
                always {
                    junit 'target/surefire-reports/*.xml'
                }
            }
        }
        stage('Static Analysis for Sonar') {
            steps {
                withSonarQubeEnv('sonarqube') {
                    sh 'mvn clean verify sonar:sonar'
                }
            }
        }
        stage('Sonar Gate') {
            steps {
                sh 'echo "sonar gate ok"'
            } 
        }
        stage('Create Package') {
            steps {
                sh 'mvn package'
		sh 'mvn install'
		sh 'ls -al target/'
                sh 'echo "publish to artifactory ok"'
            } 
        }
        stage('Publish Package') {
            steps {
                hygieiaArtifactPublishStep artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33"
                sh 'sleep 150'
            }
        }
        stage('Deploy to DEV') {
            steps {
                sh 'echo "deploy to dev ok"'
		sh 'sleep 180'
            }
            post { 
                success {
                    hygieiaDeployPublishStep applicationName: 'my-jacksonvilleapp', artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33", buildStatus: 'Success', environmentName: 'DEV'
                }
                failure {
                    hygieiaDeployPublishStep applicationName: 'my-jacksonvilleapp', artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33", buildStatus: 'Failure', environmentName: 'DEV'
                }
            }
        }
        stage('QA Canary') {
            steps {
                sh 'echo "execute qa canary"'
            }
        }
        stage('Deploy to QA') {
            steps {
                sh 'echo "deploy to qa ok"'
		sh 'sleep 180'
            }
            post { 
                success {
                    hygieiaDeployPublishStep applicationName: 'my-jacksonvilleapp', artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33", buildStatus: 'Success', environmentName: 'QA'
                }
                failure { 
                    hygieiaDeployPublishStep applicationName: 'my-jacksonvilleapp', artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33", buildStatus: 'Failure', environmentName: 'QA'
                }
            }
        }
        stage('QA Smoke') {
            steps {
                sh 'echo "execute qa smoke"'
            }
        }
        stage('Deploy to Stage') {
            steps {
                sh 'echo "deploy to stage ok"'
		sh 'sleep 180'
            }
            post { 
                success {
                    hygieiaDeployPublishStep applicationName: 'my-jacksonvilleapp', artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33", buildStatus: 'Success', environmentName: 'STAGE'
                }
                failure {
                    hygieiaDeployPublishStep applicationName: 'my-jacksonvilleapp', artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33", buildStatus: 'Failure', environmentName: 'STAGE'
                }
            }
        }
        stage('Stage Canary') {
            steps {
                sh 'echo "execute stage canary"'
            }
        }
        stage('Stage Smoke') {
            steps {
                sh 'echo "execute stage canary"'
            }
        }
        stage('Deploy to Prod') {
            steps {
                sh 'echo "deploy to production ok"'
		sh 'sleep 240'
            }
            post { 
                success {
                    hygieiaDeployPublishStep applicationName: 'my-jacksonvilleapp', artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33", buildStatus: 'Success', environmentName: 'PROD'
                }
                failure {
                    hygieiaDeployPublishStep applicationName: 'my-jacksonvilleapp', artifactDirectory: 'target', artifactGroup: 'com.jacksonville.app', artifactName: '*.jar', artifactVersion: "1.0-SNAPSHOT-YSBHYGIEIA33", buildStatus: 'Failure', environmentName: 'PROD'
                }
            }
        }
        stage('Prod Canary') {
            steps {
                sh 'echo "execute Prod canary"'
            }
        }
        stage('Prod Smoke') {
            steps {
                sh 'echo "execute Prod smoke"'
            }
        }
        stage('Pipeline End') {
            steps {
                sh 'echo "Pipeline End"'
            }
	}
    }
}
